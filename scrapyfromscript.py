__author__ = 'jimit'
from twisted.internet import reactor

from scrapy import log, signals
from scrapy.crawler import Crawler
from scrapy.utils.project import get_project_settings
from scrapy.xlib.pydispatch import dispatcher
import uuid

from kickstarter.spiders.kickstarter_spider import KickstarterSpider

def stop_reactor():
    reactor.stop()

dispatcher.connect(stop_reactor, signal=signals.spider_closed)
spider = KickstarterSpider()
settings = get_project_settings()
settings.set('FEED_URI', str(uuid.uuid4())+'.json', priority='cmdline')
settings.set('FEED_FORMAT', 'json', priority='cmdline')
crawler = Crawler(settings)
crawler.configure()
crawler.crawl(spider)
crawler.start()
log.start()
log.msg('Running reactor...')
reactor.run()  # the script will block here until the spider is closed
log.msg('Reactor stopped.')