from scrapy.selector.lxmlsel import Selector
from kickstarter.items import KickstarterItem

__author__ = 'jimit'
import scrapy

class KickstarterSpider(scrapy.Spider):
    name='kickstarter'
    allowed_domains = ["kickstarter.com"]
    start_urls = ["https://www.kickstarter.com/projects/cze/ghostbusters-the-board-game?ref=home_popular"]

    def parse(self, response):
        rec = Selector(response)
        item = KickstarterItem()

        item['title'] = rec.xpath('//*[@id="content-wrap"]/section/div[3]/div/h2/a/text()').extract()[0]
        item['image'] = rec.xpath('//*[@id="video_pitch"]/img/@src').extract()[0]
        item['category'] =rec.xpath('//*[@id="content-wrap"]/section/div[4]/div[1]/div/div[2]/div[1]/div[2]/a[2]/b/text()').extract()[0]
        item['funding_goal'] = rec.xpath('//*[@id="stats"]/div/div[2]/span/span[1]/text()').extract()[0]
        item['total_funded'] = rec.xpath('//*[@id="pledged"]/data/text()').extract()[0]
        item['creator'] = rec.xpath('//*[@id="content-wrap"]/section/div[3]/div/p[2]/b/a/text()').extract()[0]
        print item
        return item