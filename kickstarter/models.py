__author__ = 'jimit'
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

import settings

DeclarativeBase = declarative_base()

def create_kick_table(engine):
    DeclarativeBase.metadata.create_all(engine)

def db_connect():
    return create_engine(URL(**settings.DATABASE))

class projects(DeclarativeBase):
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True)
    title = Column('title',String)
    image = Column('Image_URL', String)
    category = Column('Category',String)
    funding_goal = Column('Fund_Required', String)
    total_funded = Column('Total_Funded',String)
    creator = Column('creator', String)