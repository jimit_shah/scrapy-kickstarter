# -*- coding: utf-8 -*-

# Scrapy settings for kickstarter project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'kickstarter'

SPIDER_MODULES = ['kickstarter.spiders']
NEWSPIDER_MODULE = 'kickstarter.spiders'
ITEM_PIPELINES = ['kickstarter.pipelines.KickstarterPipeline']
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'kickstarter (+http://www.yourdomain.com)'
DATABASE = {'drivername': 'postgres',
            'host': 'localhost',
            'port': '5432',
            'username': 'postgres',
            'password': 'Promact2014',
            'database': 'scrape'}